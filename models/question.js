const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const questionSchema = new Schema(
  {
    quiz: {
      type: Schema.Types.ObjectId,
      ref: "Quiz"
    },
    question: {
      type: String,
      required: true
    },
    choices: {
      A: {
        type: String,
        required: true
      },
      B: {
        type: String,
        required: true
      },
      C: {
        type: String,
        required: true
      },
      D: {
        type: String,
        required: true
      }
    },
    answer: {
      type: String,
      trim: true,
      enum: ["A", "B", "C", "D"],
      required: true
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
);

module.exports = mongoose.model("Question", questionSchema);
