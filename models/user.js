const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const uniqueValidator = require("mongoose-unique-validator");

const userSchema = new Schema(
  {
    name: {
      type: String,
      trim: true
    },
    email: {
      type: String,
      unique: true,
      uniqueCaseInsensitive: true,
      trim: true,
      required: true
    },
    role: {
      type: String,
      enum: ["user", "creator"],
      default: "user"
    },
    encrypted_password: {
      type: String,
      required: "Password is required!"
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
);

userSchema.plugin(uniqueValidator);
module.exports = mongoose.model("User", userSchema);
