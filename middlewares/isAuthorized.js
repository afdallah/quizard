const { error } = require("../helpers/handler");

module.exports = async (req, res, next) => {
  if (req.user.role !== "creator")
    return error(401, "You are not authorized to do this action");
  next();
};
