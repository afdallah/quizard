const jwt = require("jsonwebtoken");
const { error } = require("../helpers/handler");

module.exports = (req, res, next) => {
  if (!req.headers.authorization) return error(400, "Wrong headers!");

  const [prefix, token] = req.headers.authorization.split(" ");
  if (!prefix || prefix !== "Bearer") return error(400, "Wrong headers!");
  if (!token) return error(400, "No token found!");

  const decoded = jwt.verify(token, process.env.JWT_SECRET_KEY);
  if (!decoded) return error(400, "Token is invalid");

  req.user = decoded;
  next();
};
