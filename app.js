/**
 * 1. User punya 2 jenis role: Admin, user
 * 2. Admin dapat membuat quiz, question
 * 3. User dapat mengisi jawaban quiz
 * 4. User bisa mengikuti beberapa quiz berbeda
 * 5. User dapat melihat score
 */

const express = require("express");
const app = express();
const morgan = require("morgan");
const cors = require('cors');

const homeRouter = require("./routes/index");
const userRouter = require("./routes/users");
const quizRouter = require("./routes/quiz");
const questionRouter = require("./routes/question");

// Middleware
app.use(express.json());
app.use(express.static("public"));
app.use(morgan("dev"));
app.use(cors());

// Routers
app.use("/", homeRouter);
app.use("/api/v1/users", userRouter);
app.use("/api/v1/quizzes", quizRouter);
app.use("/api/v1/questions", questionRouter);

// Error handlers
app.use((err, req, res, next) => {
  let { status, statusCode, message } = err;

  statusCode = statusCode || 400;
  res.status(statusCode).json({ status, message });
});

module.exports = app;
