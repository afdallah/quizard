/**
 * Endpoints:
 * 1. GET - / => get all users
 * 2. GET - / => get user by email
 * 3. GET - /:id => get user by id
 * 4. POST - / => Create/Register user
 *    - Validasi password confirmation
 *    - Hash password dengan bcryptjs
 *    - Return created user
 * 5. POST - /login => Login/Authenticate user
 *    - Compare body.password dengan hashed password di db
 *    - Jika sama, generate token dengan jwt
 *    - Jika tidak, reject.
 * 6. PUT - /:id => Update user by id
 *    - Gunakan params.id untuk query user sekaligus update
 *    - Return updated user
 * 8. DELETE - /:id => Delete user by id
 *    - Gunakan params.id untuk query user sekaligus delete
 *    - Return deleted user
 */

const validator = require("validator");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const User = require("../models/user");
const Quiz = require("../models/quiz");
const Question = require("../models/question");
const { error, success } = require("../helpers/handler");

exports.create = async (req, res) => {
  const { password, password_confirmation } = req.body;

  if (!password) return error(400, "password is required");
  if (!password_confirmation)
    return error(400, "password_confirmation is required");
  if (!validator.equals(password, password_confirmation))
    return error(400, "password is not equals");

  const salt = await bcrypt.genSalt(10);
  const hash = await bcrypt.hash(req.body.password, salt);
  const user = await User.create({ ...req.body, encrypted_password: hash });
  const token = jwt.sign(
    { _id: user._id, role: user.role },
    process.env.JWT_SECRET_KEY
  );

  success(res, 200, { _id: user._id, email: user.email, token: token });
};

// Login
exports.login = async (req, res) => {
  const { email, password } = req.body;
  if (!password) {
    error(400, "Password is required!");
  }

  const user = await User.findOne({ email });
  if (!user) return error(400, `User with email: ${email} is not found!`);

  const isValidPassword = bcrypt.compareSync(password, user.encrypted_password);
  if (!isValidPassword) error(400, "Wrong password!");

  const token = jwt.sign(
    { _id: user._id, role: user.role },
    process.env.JWT_SECRET_KEY
  );

  success(res, 200, { ...user._doc, token: token });
};

exports.getAll = async (req, res) => {
  const users = await User.find().select("-encrypted_password");

  if (!users.length) return error(400, "No user found!");
  res.status(200).json(users);
};

// Get user by id
exports.getById = async (req, res) => {
  const id = req.params.id;
  const user = await User.findById(id).select("-encrypted_password");

  res.status(200).json({
    status: true,
    data: user
  });
};

// Update
exports.update = async (req, res) => {
  const { id } = req.params;
  if (!id) return error(400, "User id is required!");

  const user = await User.findByIdAndUpdate(
    id,
    { $set: req.body },
    { new: true }
  ).select("-encrypted_password");

  if (!user) return error("404", `User with id: ${id} not found!`);
  success(res, 200, user);
};

// Delete user
exports.delete = async (req, res) => {
  const id = req.params.id;
  if (!id) return error(400, "Please provide User id");

  const user = User.findById(id);
  if (!user) return error(404, `User with id ${id} not found!`)

  const quiz = await Quiz.findOne({ owner: id }, { new: true });
  await Question.deleteMany({ quiz: quiz._id });
  await Quiz.deleteMany({ owner: id });
  const deletedUser = await User.findByIdAndDelete(id).select(
    "-encrypted_password"
  );
  success(res, 200, deletedUser);
};
