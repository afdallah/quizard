const Quiz = require("../models/quiz");
const Question = require("../models/question");
const { success } = require("../helpers/handler");

exports.create = async (req, res) => {
  const owner = req.user._id;
  const quiz = await Quiz.create({ ...req.body, owner });
  success(res, 201, quiz);
};

exports.getAll = async (req, res) => {
  const quizzes = await Quiz.find().populate({
    path: "owner",
    select: "-encrypted_password"
  });
  success(res, 200, quizzes);
};

// get my quiz
exports.getMyQuizzes = async (req, res) => {
  const quizzes = await Quiz.find({ owner: req.user._id }).select("-owner");
  success(res, 200, quizzes);
};

// Get quiz by id
exports.getQuiz = async (req, res) => {
  const quiz = await Quiz.findById(req.params.id).populate({
    path: "owner",
    select: "-encrypted_password"
  });
  const questions = await Question.find({ quiz: quiz._id }).select("-quiz");

  success(res, 200, {
    _id: quiz._id,
    title: quiz.title,
    owner: quiz.owner,
    questions
  });
};

// Get user quizzes
exports.getUserQuizzes = async (req, res) => {
  const id = req.params.id;
  const quizzes = await Quiz.find({ owner: id })

  success(res, 200, quizzes);
};

// Update quiz
exports.updateQuiz = async (req, res) => {
  const id = req.params.id;
  const quiz = await Quiz.findByIdAndUpdate(
    id,
    {
      $set: req.body
    },
    { new: true }
  );

  success(res, 200, quiz);
};

// Delete quiz
exports.delete = async (req, res) => {
  const id = req.params.id
  const deleted = await Quiz.findOneAndDelete(id)

  success(res, 200, deleted)
}
