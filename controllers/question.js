const Question = require("../models/question");
const Quiz = require("../models/quiz");

const { success, error } = require("../helpers/handler");

exports.create = async (req, res) => {
  const question = await Question.create(req.body);

  success(res, 200, question);
};

exports.getAll = async (req, res) => {
  const questions = await Question.find().select("-answer");

  success(res, 200, questions);
};

exports.getById = async (req, res) => {
  const quiz_id = req.params.id;
  const questions = await Question.find({ quiz: req.params.id });
  const quiz = await Quiz.findById(quiz_id)
    .select("-answer")
    .populate({
      path: "owner",
      select: "-encrypted_password"
    });

  success(res, 200, {
    title: quiz.title,
    owner: quiz.owner,
    questions
  });
};

exports.update = async (req, res) => {
  const id = req.params.id;
  const updated = await Question.findByIdAndUpdate(
    id,
    { $set: req.body },
    { new: true }
  );

  success(res, 200, updated);
};

// Answer questions
exports.answer = async (req, res) => {
  const quiz_id = req.params.id;
  const { questions: answers } = req.body;
  const questions = await Question.find({ quiz: quiz_id });
  let correct = 0;

  // kita loop questions nya
  // kita dapet object
  // Cocokan pertanyaan
  // di dalem object kita bisa akses answer
  // compare dengan answer dari user (req.body)
  for (let i = 0; i < questions.length; i++) {
    if (questions[i]._id == answers[i].question_id) {
      if (questions[i].answer === answers[i].answer) {
        correct += 1;
      }
    }
  }

  scores = (correct / questions.length) * 100;
  success(res, 200, {
    results: `${correct} correct answer of ${questions.length} questions`,
    scores
  });
};
