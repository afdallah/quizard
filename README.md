## [Live Demo](https://quizards.herokuapp.com/)

## Users
- Create a new user

POST: /api/v1/users

**body**
```
{
    "name": "Dallah",
    "email": "dal@gmail.com",
    "password": "satu",
    "password_confirmation": "satu",
    "role": "creator"
}
```

- Login

POST: /api/v1/users/login

```
{
	"email": "dal@gmail.com",
	"password": "satu"
}
```

- Get all users

GET: /api/v1/users

- Get user by id

GET: /api/v1/users/:id

- Delete users by id

DELETE: /api/v1/users/:id

- Udate user by id

PUT: /api/v1/users/:id

## Quiz

- Create Question
POST: /api/v1/questions

**body**

```
{
	"title": "Quiz Sepeda Jokowi",
	"description": "Ayo ikut kuis aja, saya ngga galak galak amat kok",
	"owner": "5e3f1409aa86bcd7fc900407"
}
```

- Update quiz by id

PUT: /api/v1/quizzes/:id

**example body**
```
{
	"title": "Quiz Sepeda Jokowi",
	"description": "Ayo ikut kuis aja, saya ngga galak galak amat kok",
	"owner": "5e3f1409aa86bcd7fc900407"
}
```

- Delete by quiz id

DELETE: /api/v1/quizzes/:id

- Get all quizzes

GET: /api/v1/quizzes/

- Get my quiz

GET: /api/v1/quizzes/mine

- Get quiz by id

GET: /api/v1/quizzes

- Get user's quizzes

GET: /api/v1/quizzes/users/:id

## Questions

- Create question

POST: /api/v1/questions

```
{
	"quiz": "5e3f3dccea37a4cd1da98ddc",
	"question": "Sebutkan nama nama ikan",
	"choices": {
		"A": "Ikan PA' US",
		"B": "Ikan Rere",
		"C": "Ikan Indosiar",
		"D": "Ikan KNTL"
	},
	"answer": "D"
}
```

- Update question

PUT: /api/v1/questions

- Get all questions

GET: /api/v1/questions

- Get question by id

GET: /api/v1/questions/:id

## Answer

- Answer questions for  a quiz

POST: /api/v1/quizzes/:id/answer

```
{
	"questions": [
		{
			"question_id": "5e3f18f35136d30e64140ad9",
			"answer": "A"
		},{
			"question_id": "5e3f18f45136d30e64140ada",
			"answer": "D"
		}
	]
}
```
