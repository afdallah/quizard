class CreateError extends Error {
  constructor(statusCode, message, status = false) {
    super();
    this.status = status;
    this.statusCode = statusCode;
    this.message = message;
  }
}

exports.success = (res, status, data) => {
  res.status(status).json({
    status: true,
    data
  });
};

exports.error = (statusCode, message, status) => {
  throw new CreateError(statusCode, message, status);
};
