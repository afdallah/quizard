const express = require("express");
const router = express.Router();

const quizController = require("../controllers/quiz");
const questionController = require("../controllers/question");
const isAuthenticated = require("../middlewares/isAuthenticated");
const isAuthorized = require("../middlewares/isAuthorized");

router.post("/", isAuthenticated, isAuthorized, quizController.create);
router.get("/", quizController.getAll);
router.get("/mine", isAuthenticated, quizController.getMyQuizzes);
router.get("/:id", quizController.getQuiz);
router.get("/users/:id", quizController.getUserQuizzes);
router.put("/:id", quizController.updateQuiz);
router.delete("/:id", quizController.delete);
router.post("/:id/answer", questionController.answer);

module.exports = router;
