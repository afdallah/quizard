const express = require("express");
const router = express.Router();

const userRouter = require("../controllers/user");

router.post("/", userRouter.create);
router.post("/login", userRouter.login);
router.get("/", userRouter.getAll);
router.get("/:id", userRouter.getById);
router.put("/:id", userRouter.update);
router.delete("/:id", userRouter.delete);

module.exports = router;
