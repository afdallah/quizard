const express = require("express");
const router = express.Router();

const questionController = require("../controllers/question");

router.post("/", questionController.create);
router.get("/", questionController.getAll);
router.get("/:id", questionController.getById);
router.put("/:id", questionController.update);

module.exports = router;
